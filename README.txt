
-- SUMMARY --

This is an implementation of John Conway's Game of Life
(see here: http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) for
 Drupal. This puts the Game of Life in a block for use in the sidebar,
for example.

For a full description of the module, visit the project page:
  http://drupal.org/project/conwayslife

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/conwayslife


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* (Optional, but useless without): In the Blocks config (Administer -> Site Building -> Blocks), move the new Conway's Game of Life block to
  wherever you want it.

* (Optional): Configure the module to suit your layout and theme in the admin
  page (Administer -> Site configuration -> Conway's Game of Life)


-- CONTACT --

Current maintainers:
* Steve Babineau (nomachinez) - http://drupal.org/user/283056


