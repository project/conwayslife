<?


  chdir(realpath('../../'));

  require_once('./includes/bootstrap.inc');
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

  $image_size = variable_get('conwayslife_imagesize', '400x400');
  list($image_w, $image_h) = explode('x', $image_size, 2);

  $cell_w = variable_get('conwayslife_cellsize', '4');
  $cell_h = $cell_w;

  $bg_hex = variable_get('conwayslife_bgcolor', '#000000');
  $fg_hex = variable_get('conwayslife_fgcolor', '#306080');
  $txtbg_hex = variable_get('conwayslife_txtbgcolor', '#000000');
  $txtfg_hex = variable_get('conwayslife_txtfgcolor', '#C0C000');

  $bg[0] = hexdec(substr($bg_hex, 1, 2));
  $bg[1] = hexdec(substr($bg_hex, 3, 2));
  $bg[2] = hexdec(substr($bg_hex, 5, 2));
  $bg[3] = (variable_get('conwayslife_bgcolor_alpha', '0') / 100) * 127;

  $fg[0] = hexdec(substr($fg_hex, 1, 2));
  $fg[1] = hexdec(substr($fg_hex, 3, 2));
  $fg[2] = hexdec(substr($fg_hex, 5, 2));
  $fg[3] = (variable_get('conwayslife_fgcolor_alpha', '0') / 100) * 127;

  $txt_bg[0] = hexdec(substr($txtbg_hex, 1, 2));
  $txt_bg[1] = hexdec(substr($txtbg_hex, 3, 2));
  $txt_bg[2] = hexdec(substr($txtbg_hex, 5, 2));
  $txt_bg[3] = (variable_get('conwayslife_txtbgcolor_alpha', '50') / 100) * 127;

  $txt_fg[0] = hexdec(substr($txtfg_hex, 1, 2));
  $txt_fg[1] = hexdec(substr($txtfg_hex, 3, 2));
  $txt_fg[2] = hexdec(substr($txtfg_hex, 5, 2));
  $txt_fg[3] = (variable_get('conwayslife_txtfgcolor_alpha', '0') / 100) * 127;

  $showtext = variable_get('conwayslife_showtext', 1);

  // ---------------------------------------------------
  // Program... START
  // ---------------------------------------------------
  $base_image = imagecreatetruecolor($image_w, $image_h);

  $bg_col     = imagecolorallocatealpha($base_image, $bg[0], $bg[1], $bg[2], $bg[3]);
  $fg_col     = imagecolorallocatealpha($base_image, $fg[0], $fg[1], $fg[2], $fg[3]);
  $txt_bg_col = imagecolorallocatealpha($base_image, $txt_bg[0], $txt_bg[1], $txt_bg[2], $txt_bg[3]);
  $txt_fg_col = imagecolorallocatealpha($base_image, $txt_fg[0], $txt_fg[1], $txt_fg[2], $txt_fg[3]);

  $data['generations'] = variable_get('conwayslife_generations', 0);
  $data['life']        = variable_get('conwayslife_life', array());

  $board_w = $image_w / $cell_w;
  $board_h = $image_h / $cell_h;


  if ($_GET['reset'] == '1' || count($data['life']) <= 0)
  {
    makenewworld();
    makeworldfromdata(&$base_image);
  }
  else
  {
    if (do_conways() == 0)
    {
      makenewworld();
    }

    makeworldfromdata(&$base_image);
  }

  if ($showtext) draw_txt(&$base_image, $data['generations'], $txt_fg_col, $txt_bg_col);

  update_data();

  //  Output
  header("Content-type: image/png");
  imagepng($base_image);
  
  //  Clean up
  imagedestroy($base_image);

  // ---------------------------------------------------
  // FUNCTIONS
  // ---------------------------------------------------

  function do_conways()
  {
    global $board_w, $board_h, $data;
    $changed = 0;

    $life = $data['life'];

    for ($x = 0; $x < $board_w; $x++)
    {
      for ($y = 0; $y < $board_h; $y++)
      {
        $sur_cells = numlivesurroundingcells($x, $y);
        if ($data['life'][$x][$y])
        {
          if ($sur_cells > 3 || $sur_cells < 2)
          {
            // uh oh :(
            $life[$x][$y] = 0;
            $changed++;
          }
        }
        elseif ($sur_cells == 3)
        {
          // new life!
          $life[$x][$y] = 1;
          $changed++;
        }
      }
    }
    $data['life'] = $life;

    return $changed;
  }

  function draw_txt($img, $gen, $fg_col, $bg_col)
  {
    global $image_w, $image_h;
    $font = 3;

    $gen_str = "Generations: " . $gen;
    imagefilledrectangle($img, 0, 0, $image_w, imagefontheight($font) + 4, $bg_col);
    $left = $image_w / 2 - strlen($gen_str) * imagefontwidth($font) / 2;
    imagestring($img, $font, $left, 2, $gen_str, $fg_col);

    $footer_str = "23/3";

    imagefilledrectangle($img, 0, $image_h - imagefontheight($font) - 4, $image_w, $image_h, $bg_col);
    $left = $image_w / 2 - strlen($footer_str) * imagefontwidth($font) / 2;
    imagestring($img, $font, $left, $image_h - imagefontheight($font) - 2, $footer_str, $fg_col);
  }
  function update_data()
  {
    global $life_meta_file, $data;

    $data['generations']++;

    variable_set('conwayslife_life', $data['life']);
    variable_set('conwayslife_generations', $data['generations']);
  }
  function makenewworld()
  {
    global $board_w, $board_h, $data;

    $life = array();

    for ($x = 0; $x < $board_w; $x++)
    {
      $life[] = array();
      for ($y = 0; $y < $board_h; $y++)
      {
        if (rand(0, 1))
          $life[$x][] = rand(0, 1);
        else
          $life[$x][] = 0;
      }
    }
    $data['life'] = $life;
    $data['generations'] = 0;
  }

  function makeworldfromdata($image)
  {
    global $image_w, $image_h, $cell_w, $cell_h, $fg_col, $data;

    for ($x = 0; $x < $image_w; $x++)
    {
      for ($y = 0; $y < $image_h; $y++)
      {
        if ($data['life'][$x][$y])
        {
          $x1 = $x * $cell_w;
          $y1 = $y * $cell_h;
          imagefilledrectangle($image, $x1, $y1, $x1 + $cell_w - 1, $y1 + $cell_h - 1, $fg_col);
        }
      }
    }
  }

  function numlivesurroundingcells($x, $y)
  {
    global $data;
    $surrounding_cells = 0;

    // top left
    $surrounding_cells += $data['life'][$x - 1][$y - 1];

    // top
    $surrounding_cells += $data['life'][$x][$y - 1];

    // top right
    $surrounding_cells += $data['life'][$x + 1][$y - 1];

    // right
    $surrounding_cells += $data['life'][$x + 1][$y];

    // bottom right
    $surrounding_cells += $data['life'][$x + 1][$y + 1];

    // bottom
    $surrounding_cells += $data['life'][$x][$y + 1];

    // bottom left
    $surrounding_cells += $data['life'][$x - 1][$y + 1];

    // left
    $surrounding_cells += $data['life'][$x - 1][$y];

    return $surrounding_cells;
  }

?>
